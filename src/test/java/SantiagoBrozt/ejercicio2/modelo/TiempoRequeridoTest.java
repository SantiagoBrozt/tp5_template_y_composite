package SantiagoBrozt.ejercicio2.modelo;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TiempoRequeridoTest {

    @Test
    public void tiempoRequerdo() {
        ItemDeTrabajo s1 = new Spike(Duration.ofHours(4));
        ItemDeTrabajo s2 = new Spike(Duration.ofHours(10));
        ItemDeTrabajo s3 = new Spike(Duration.ofMinutes(140));
        ItemDeTrabajo h1 = new HistoriaDeUsuario(s1, s2, s3);
        var tiempoRequerido = h1.tiempoRequerido();
        assertEquals(Duration.ofHours(16).plusMinutes(20), tiempoRequerido);
    }
}
