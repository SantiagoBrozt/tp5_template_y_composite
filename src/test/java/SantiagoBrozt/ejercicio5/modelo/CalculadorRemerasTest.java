package SantiagoBrozt.ejercicio5.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculadorRemerasTest {

    @Test
    public void remeraNacional() {
        var remeraNacional = new RemeraNacional(100);
        double resultado = remeraNacional.precioFinal();
        assertEquals(93.725, resultado, 0.1);
    }

    @Test
    public void remeraImportada() {
        var remeraNacional = new RemeraImportada(100);
        double resultado = remeraNacional.precioFinal();
        assertEquals(135, resultado, 0.1);
    }
}
