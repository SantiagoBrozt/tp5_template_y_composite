package SantiagoBrozt.ejercicio4.modelo;

import org.junit.jupiter.api.Test;

import static java.time.LocalDate.now;
import static org.junit.jupiter.api.Assertions.*;

public class CalculadorTest {

    @Test
    public void jubiladoSinPromocion(){
        CalculadorJubilado calculador = new CalculadorJubilado(
                now().getMonth().getValue() - 1,
                new LogTransaction());
        double resultado = calculador.calcularPrecio(100);
        assertEquals(110, resultado,0.01);
    }

    @Test
    public void jubiladoConPromocion(){
        CalculadorJubilado calculador = new CalculadorJubilado(
                now().getMonth().getValue(),
                new LogTransaction());
        double resultado = calculador.calcularPrecio(100);
        assertEquals(100, resultado,0.01);
    }

    @Test
    public void NoJubiladoSinPromocion(){
        CalculadorNoJubilado calculador = new CalculadorNoJubilado(
                now().getMonth().getValue() -1,
                new LogTransaction());
        double resultado = calculador.calcularPrecio(100);
        assertEquals(121, resultado,0.01);
    }

    @Test
    public void NoJubiladoConPromocion(){
        CalculadorNoJubilado calculador = new CalculadorNoJubilado(
                now().getMonth().getValue(),
                new LogTransaction());
        double resultado = calculador.calcularPrecio(100);
        assertEquals(115, resultado,0.01);
    }
}
