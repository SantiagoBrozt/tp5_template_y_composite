package SantiagoBrozt.ejercicio1.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MontoSalarialTest {

    @Test
    public void montoSalarial() {
        var empresa = new Empresa();
        var director = new EmplepleadoACargo(EmplepleadoACargo.DIRECTOR);
        var gerente = new EmplepleadoACargo(EmplepleadoACargo.GERENTE);
        var mandoMedio1 = new EmplepleadoACargo(EmplepleadoACargo.MANDO_MEDIO);
        var mandoMedio2 = new EmplepleadoACargo(EmplepleadoACargo.MANDO_MEDIO);
        var lider1 = new EmplepleadoACargo(EmplepleadoACargo.LIDER_DE_PROYECTO);
        var lider2 = new EmplepleadoACargo(EmplepleadoACargo.LIDER_DE_PROYECTO);
        var lider3 = new EmplepleadoACargo(EmplepleadoACargo.LIDER_DE_PROYECTO);
        var empleado1 = new EmpleadoRegular();
        var empleado2 = new EmpleadoRegular();
        var empleado3 = new EmpleadoRegular();
        var empleado4 = new EmpleadoRegular();
        empresa.nuevosEmpleados(director);
        director.asignarEmpleados(gerente);
        gerente.asignarEmpleados(mandoMedio1, mandoMedio2);
        mandoMedio1.asignarEmpleados(lider1, lider2);
        mandoMedio2.asignarEmpleados(lider3);
        lider1.asignarEmpleados(empleado1, empleado2);
        lider2.asignarEmpleados(empleado3, empleado4);
        int montoSalarial = empresa.montoSalarial();
        assertEquals(7000, montoSalarial);
    }
}
