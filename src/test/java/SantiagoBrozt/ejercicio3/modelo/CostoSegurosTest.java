package SantiagoBrozt.ejercicio3.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CostoSegurosTest {

    @Test
    public void costoPaqueteSeguros() {
        var seguroHogar = new SeguroParticular(SeguroParticular.HOGAR);
        var seguroAutomovil = new SeguroParticular(SeguroParticular.AUTOMOVIL);
        var seguroDeVida = new SeguroParticular(SeguroParticular.VIDA);
        var seguroMedico = new SeguroParticular(SeguroParticular.MEDICO);
        var p1 = new PaqueteSeguros();
        var p2 = new PaqueteSeguros();
        p1.aniadirSeguro(seguroHogar, seguroMedico);
        p2.aniadirSeguro(p1, seguroAutomovil, seguroDeVida);
        var costoPaquete2 = p2.costo();
        assertEquals(3240,costoPaquete2);
    }
}
