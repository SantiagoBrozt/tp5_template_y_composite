package SantiagoBrozt.ejercicio2.modelo;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class HistoriaDeUsuario implements ItemDeTrabajo {
    private List<ItemDeTrabajo> tareas = new ArrayList<>();

    public HistoriaDeUsuario(ItemDeTrabajo... tareas) {
        this.tareas.addAll(List.of(tareas));
    }

    @Override
    public Duration tiempoRequerido() {
        Duration tiempoRequerido = Duration.ZERO;
        for(ItemDeTrabajo i : tareas) {
            tiempoRequerido = tiempoRequerido.plus(i.tiempoRequerido());
        }
        return tiempoRequerido;
    }
}
