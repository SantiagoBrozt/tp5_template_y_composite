package SantiagoBrozt.ejercicio2.modelo;

import java.time.Duration;

public class Spike implements ItemDeTrabajo{
    private Duration tiempo;

    public Spike(Duration tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public Duration tiempoRequerido() {
        return tiempo;
    }
}
