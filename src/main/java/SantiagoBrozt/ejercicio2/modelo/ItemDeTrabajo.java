package SantiagoBrozt.ejercicio2.modelo;

import java.time.Duration;

public interface ItemDeTrabajo {
    public Duration tiempoRequerido();
}
