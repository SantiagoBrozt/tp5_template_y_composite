package SantiagoBrozt.ejercicio1.modelo;

import java.util.ArrayList;
import java.util.List;

public class Empresa {
    private List<Empleado> Directores;

    public Empresa() {
        this.Directores = new ArrayList<>();
    }

    public void nuevosEmpleados(Empleado... empleados) {
        this.Directores.addAll(List.of(empleados));
    }

    public int montoSalarial() {
        var montoSalarial = 0;
        for (Empleado e : Directores) {
            montoSalarial += e.montoSalarialACargo();
        }
        return montoSalarial;
    }
}
