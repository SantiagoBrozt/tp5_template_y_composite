package SantiagoBrozt.ejercicio1.modelo;

public interface Empleado {
    public int cuantoCobra();
    public int montoSalarialACargo();
}
