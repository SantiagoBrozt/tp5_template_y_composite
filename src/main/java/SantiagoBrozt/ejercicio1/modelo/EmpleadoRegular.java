package SantiagoBrozt.ejercicio1.modelo;

public class EmpleadoRegular implements Empleado {
    private static final int salario = 350;

    @Override
    public int cuantoCobra() {
        return salario;
    }

    @Override
    public int montoSalarialACargo() {
        return cuantoCobra();
    }
}
