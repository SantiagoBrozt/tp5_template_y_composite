package SantiagoBrozt.ejercicio1.modelo;

import java.util.ArrayList;
import java.util.List;

public class EmplepleadoACargo implements Empleado {
    public static final int DIRECTOR = 0;
    public static final int GERENTE = 1;
    public static final int MANDO_MEDIO = 2;
    public static final int LIDER_DE_PROYECTO = 3;
    private int categoria;
    private List<Empleado> empleados;

    public EmplepleadoACargo(int categoria) {
        this.categoria = categoria;
        empleados = new ArrayList<>();
    }

    public void asignarEmpleados(Empleado ... empleados) {
        this.empleados.addAll(List.of(empleados));
    }

    @Override
    public int cuantoCobra() {
        var salario = 0;
        if (this.categoria == DIRECTOR) {
            salario = 1500;
        }
        if (this.categoria == GERENTE) {
            salario = 1000;
        }
        if (this.categoria == MANDO_MEDIO) {
            salario = 800;
        }
        if (this.categoria == LIDER_DE_PROYECTO) {
            salario = 500;
        }
        return salario;
    }

    @Override
    public int montoSalarialACargo() {
        var montoSalarial = 0;
        for (Empleado e : empleados) {
            montoSalarial += e.montoSalarialACargo();
        }
        return montoSalarial += cuantoCobra();
    }
}
