package SantiagoBrozt.ejercicio5.modelo;

public class RemeraNacional extends Remera{
    public RemeraNacional(double precioUnitario) {
        super(precioUnitario);
    }

    @Override
    protected double recargosExternos(double precioUnitario) {
        return precioUnitario
                + precioUnitario * 0.015
                - precioUnitario * 0.2;
    }

    @Override
    protected double recargoComercio(double precio) {
        return precio+ (precio * 0.15);
    }

//    public double precioTotal() {
//        double precioFinal = this.precioUnitario
//                + this.precioUnitario * 0.015
//                - this.precioUnitario * 0.2;
//        precioFinal += precioFinal * 0.15;
//        return precioFinal;
//    }
}
