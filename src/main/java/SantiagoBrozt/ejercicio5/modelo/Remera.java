package SantiagoBrozt.ejercicio5.modelo;

public abstract class Remera {
    private double precioUnitario;

    public Remera(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double precioFinal() {
        double precioFinal = this.recargosExternos(this.precioUnitario);
        precioFinal = this.recargoComercio(precioFinal);
        return precioFinal;
    }

    protected abstract double recargosExternos(double precioUnitario);

    protected abstract double recargoComercio(double precio);
}
