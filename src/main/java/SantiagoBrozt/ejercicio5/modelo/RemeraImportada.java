package SantiagoBrozt.ejercicio5.modelo;

public class RemeraImportada extends Remera{
    public RemeraImportada(double precioUnitario) {
        super(precioUnitario);
    }

    @Override
    protected double recargosExternos(double precioUnitario) {
        return precioUnitario
                + precioUnitario * 0.03
                + precioUnitario * 0.05;
    }

    @Override
    protected double recargoComercio(double precio) {
        return precio + (precio * 0.25);
    }


//    public double precioTotal() {
//        double precioFinal = this.precioUnitario
//                + this.precioUnitario * 0.03
//                + this.precioUnitario * 0.05;
//        precioFinal += precioFinal * 0.25;
//        return precioFinal;
//    }
}
