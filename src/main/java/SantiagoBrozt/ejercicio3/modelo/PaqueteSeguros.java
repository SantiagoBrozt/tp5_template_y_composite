package SantiagoBrozt.ejercicio3.modelo;

import java.util.ArrayList;
import java.util.List;

public class PaqueteSeguros implements Seguro {
    private List<Seguro> seguros;

    public PaqueteSeguros() {
        seguros = new ArrayList<>();
    }

    public void aniadirSeguro(Seguro... seguros) {
        this.seguros.addAll(List.of(seguros));
    }


    @Override
    public double costo() {
        var costoTotal = 0.0;
        var descuento = 0.0;
        for (Seguro s : seguros) {
            costoTotal += s.costo();
            if(!s.sosPaquete()){
                descuento += 0.05;
            }

        }
        return costoTotal - (costoTotal * descuento);
    }

    @Override
    public boolean sosPaquete() {
        return true;
    }
}
