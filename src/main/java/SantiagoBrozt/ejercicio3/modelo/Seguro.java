package SantiagoBrozt.ejercicio3.modelo;

public interface Seguro {
    public double costo();
    public boolean sosPaquete();
}
