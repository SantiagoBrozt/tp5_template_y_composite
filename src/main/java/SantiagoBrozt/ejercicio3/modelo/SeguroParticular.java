package SantiagoBrozt.ejercicio3.modelo;

public class SeguroParticular implements Seguro {
    public static final int HOGAR = 0;
    public static final int AUTOMOVIL = 1;
    public static final int VIDA = 2;
    public static final int MEDICO = 3;
    private int tipo;

    public SeguroParticular(int tipo) {
        this.tipo = tipo;
    }
    @Override
    public double costo() {
        var costo = 0.0;
        if (this.tipo == HOGAR) {
            costo = 1500.0;
        }
        if (this.tipo == AUTOMOVIL) {
            costo = 1000.0;
        }
        if (this.tipo == VIDA) {
            costo = 800.0;
        }
        if (this.tipo == MEDICO) {
            costo = 500.0;
        }
        return costo;
    }

    @Override
    public boolean sosPaquete() {
        return false;
    }
}
