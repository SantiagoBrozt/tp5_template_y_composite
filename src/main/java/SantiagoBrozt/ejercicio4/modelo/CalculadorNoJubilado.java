package SantiagoBrozt.ejercicio4.modelo;

import static java.time.LocalDate.now;

public class CalculadorNoJubilado extends Calculador {
    public CalculadorNoJubilado(int mesEnPromocion, LogTransaction log) {
        super(mesEnPromocion, log);
    }

    @Override
    protected double mesConPromocion(double precioProducto) {
        return (precioProducto * 0.15) + precioProducto;
    }

    @Override
    protected double mesSinPromocion(double precioProducto) {
        return (precioProducto * 0.21) + precioProducto;
    }
//    private int mesEnPromocion;
//    private LogTransaction log;
//
//    public CalculadorNoJubilado(int mesEnPromocion, LogTransaction log) {
//        this.mesEnPromocion = mesEnPromocion;
//        this.log = log;
//    }
//
//    public double calcularPrecio(double precioProducto) {
//        double precioTotal = precioProducto;
//        if (of(mesEnPromocion).equals(now().getMonth())) {
//            precioTotal += precioProducto * 0.15;
//        } else {
//            precioTotal += precioProducto * 0.21;
//        }
//        log.log(CalculadorNoJubilado.class.getName());
//        return precioTotal;
//    }
}
