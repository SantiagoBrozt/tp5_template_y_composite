package SantiagoBrozt.ejercicio4.modelo;

import static java.time.LocalDate.now;

public class CalculadorJubilado extends Calculador {
    public CalculadorJubilado(int mesEnPromocion, LogTransaction log) {
        super(mesEnPromocion, log);
    }

    @Override
    protected double mesConPromocion(double precioProducto) {
        return precioProducto;
    }

    @Override
    protected double mesSinPromocion(double precioProducto) {
        return (precioProducto * 0.1) + precioProducto;
    }
//    private int mesEnPromocion;
//    private LogTransaction log;
//
//    public CalculadorJubilado(int mesEnPromocion, LogTransaction log) {
//        this.mesEnPromocion = mesEnPromocion;
//        this.log = log;
//    }
//
//    public double calcularPrecio(double precioProducto) {
//        double precioTotal = precioProducto;
//        if (!of(mesEnPromocion).equals(now().getMonth())) {
//            precioTotal += precioProducto * 0.1;
//        }
//        log.log(CalculadorJubilado.class.getName());
//        return precioTotal;
//    }

}